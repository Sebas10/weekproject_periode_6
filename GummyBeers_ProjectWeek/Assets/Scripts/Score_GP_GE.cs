﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score_GP_GE : MonoBehaviour
{
    private int m_Score;

    [SerializeField]
    private TMPro.TextMeshProUGUI m_ScorePro;

    private void Start()
    {
        m_Score = UserData.m_Score;
    }

    private void Update()
    {
        m_Score = UserData.m_Score;

        //m_ScorePro.text = "Score: " + m_Score;

        Debug.Log(m_Score);
    }

    public IEnumerator ScorePlus(int amount)
    {
        m_Score += amount;       
        Debug.Log(m_Score.ToString());
        yield return new WaitForSeconds(0.2f);
    }
}
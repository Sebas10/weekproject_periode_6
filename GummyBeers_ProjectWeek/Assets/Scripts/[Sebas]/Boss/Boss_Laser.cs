﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Laser : MonoBehaviour
{
    [SerializeField] private GameObject m_Lasers;//Damage
    [SerializeField] private GameObject m_LaserMarker;//Marker

    [SerializeField] private float m_Time_To_Shoot;
    private float m_Timer;

    void Start()
    {
        m_Lasers.SetActive(false);
        m_LaserMarker.SetActive(false);
    }

    void Update()
    {
        m_Timer += Time.deltaTime;

        if (m_Timer >= m_Time_To_Shoot)
        {
            StartCoroutine(Shooting());
        }
    }

    /// <summary>
    /// Controls what to set active and what not
    /// </summary>
    /// <returns></returns>
    private IEnumerator Shooting()
    {
        m_LaserMarker.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        m_Lasers.SetActive(true);
        m_LaserMarker.SetActive(false);
        yield return new WaitForSeconds(1f);
        m_Lasers.SetActive(false);
        m_Timer = 0;
    }
}

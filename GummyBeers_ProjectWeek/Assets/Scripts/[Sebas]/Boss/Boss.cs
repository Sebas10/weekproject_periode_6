﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    [SerializeField] private Image m_Boss_Image;
    [SerializeField] private GameObject m_BossObject;

    public bool m_Activated = false;

    void Start()
    {
        if (m_Boss_Image == null)
            Debug.LogError("No Boss Image!");

        m_Boss_Image.fillAmount = 0;

        m_BossObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Boss_Image.fillAmount == 1)
        {
            m_BossObject.SetActive(true);
            m_Activated = true;
        }
            
    }

    public void AddAmount(float amount)
    {
        if(m_Activated == false)
            m_Boss_Image.fillAmount += amount;
    }

    public void ResetDied()
    {
        if (m_Activated == false)
            m_Boss_Image.fillAmount = 0;
    }

    public void DamageTaken(float amount)
    {
        if (m_Activated == false)
            m_Boss_Image.fillAmount -= amount;
    }
    public void Win()
    {
        SceneManager.LoadScene("Win");
    }
}

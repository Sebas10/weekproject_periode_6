﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : MonoBehaviour
{
    [SerializeField] private int m_Amount_Damage;

    private PlayerHealth m_PH;

    private void Start()
    {
        m_PH = GameObject.FindObjectOfType<PlayerHealth>();

        if (m_PH == null)
            Debug.LogError("No Player health script found");
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerHealth playerhealth = other.GetComponent<PlayerHealth>();

            playerhealth.TakingDamage(m_Amount_Damage);
        }
    }
}

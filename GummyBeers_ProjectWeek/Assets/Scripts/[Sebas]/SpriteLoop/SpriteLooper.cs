﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteLooper : MonoBehaviour
{
    [SerializeField] private List<Sprite> m_Sprites;

    private SpriteRenderer m_SpriteRenderer;

    private float m_Time = 0;

    [SerializeField] private float m_MaxTime;

    private int m_CurrentSpriteIndex = 0;


    void Start()
    {

        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        if (m_Sprites.Count < 1)
        {
            Debug.LogError("You need to assign sprites here");
            Destroy(this);
        }

    }

    void Update()
    {
        LooperSprites();
    }

    private void LooperSprites()
    {
        m_Time += Time.deltaTime;

        int currentSpriteIndex = Mathf.FloorToInt(m_Time / 0.5f) % m_Sprites.Count;
        if (m_Time > m_MaxTime)
        {
            m_SpriteRenderer.sprite = m_Sprites[m_CurrentSpriteIndex];
            m_Time = 0.0f;
            m_CurrentSpriteIndex++;
            if (m_CurrentSpriteIndex >= m_Sprites.Count)
            {
                m_CurrentSpriteIndex = 0;
            }
        }
    }
}

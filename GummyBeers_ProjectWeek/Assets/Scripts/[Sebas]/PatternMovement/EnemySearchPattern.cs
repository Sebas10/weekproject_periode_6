﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySearchPattern : MonoBehaviour
{
    [SerializeField] private GameObject m_WayPointPrefab;

    private List<Transform> m_WayPoints;

    private List<string> m_RandomWaypointName;

    private Transform m_TargetWaypoint;
    private int m_CurrentWaypoint;

    [SerializeField] private float m_MaxDist;
    [SerializeField] private float m_Speed;

    [SerializeField] private EnemyPool m_EnemyPool;

    void Start()
    {
        m_WayPoints = new List<Transform>();
        foreach (Transform t in m_WayPointPrefab.transform)
        {
            m_WayPoints.Add(t);
        }

        if (m_WayPoints.Count == 0)
            Debug.LogError("Geen waypoints gevonden");

        m_CurrentWaypoint = 0;
        m_TargetWaypoint = m_WayPoints[m_CurrentWaypoint];

        m_EnemyPool.gameObject.GetComponent<EnemyPool>();

        transform.position = m_WayPoints[0].position;
    }

    void Update()
    {
        if (m_WayPoints == null)
            return;

        LookAtWaypoint();
        MovementEnemy();
    }

    /// <summary>
    /// Go to next waypoint in array
    /// </summary>
    /// <returns></returns>
    private IEnumerator NextWaypoint()
    {
        if (m_CurrentWaypoint < m_WayPoints.Count)
        {
            m_CurrentWaypoint += 1;
            yield return new WaitForSeconds(0.25f);
        }
    }

    /// <summary>
    /// Check for waypoints and move to them
    /// </summary>
    private void MovementEnemy()
    {
        if (m_CurrentWaypoint < m_WayPoints.Count)
        {
            m_TargetWaypoint = m_WayPoints[m_CurrentWaypoint];

            //As long as distance between waypoint and enemy is bigger then max dist move towards waypoint
            if (Vector3.Distance(this.transform.position, m_TargetWaypoint.position) >= m_MaxDist)
            {
                transform.position = Vector3.MoveTowards(this.transform.position, m_TargetWaypoint.position, m_Speed * Time.deltaTime);
            }
            else// Go to next waypoint
                StartCoroutine(NextWaypoint());
        }
        else
        {
            //Ayoub je moet hier je pulling gebruiken

            m_EnemyPool.ReturnToPool();
        }
    }

    /// <summary>
    /// Rotate Enemy to look at the target waypoint
    /// </summary>
    private void LookAtWaypoint()
    {
        Vector3 delta = m_TargetWaypoint.position - transform.position;

        float angle = Mathf.Atan2(delta.y, delta.x) * 180.0f / Mathf.PI;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle - 90.0f), 0.1f);
    }
}

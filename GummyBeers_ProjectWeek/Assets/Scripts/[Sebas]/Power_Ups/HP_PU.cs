﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP_PU : MonoBehaviour
{
    private PlayerHealth m_PH;

    void Start()
    {
        m_PH = GameObject.FindObjectOfType<PlayerHealth>();

        if (m_PH == null)
            Debug.LogError(gameObject.name + " Doesn't have a Playerhealth script");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            this.gameObject.SetActive(false);
        }            
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_PU : MonoBehaviour
{
    private Fire m_FireRate;

    private int m_CurrentFireRate = 0;

    private void Start()
    {
        m_FireRate = GameObject.FindObjectOfType<Fire>();

        if (m_FireRate == null)
            Debug.LogError(gameObject.name + " Missing Fire Script");
    }

    private void Update()
    {
        if(m_CurrentFireRate > 2)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            UpgradeGun();
            gameObject.SetActive(false);
        }

    }

    private void UpgradeGun()
    {
        m_CurrentFireRate += 1;
        if (m_CurrentFireRate == 1)
            m_FireRate.m_AmountGuns = 1;
        else if (m_CurrentFireRate == 2)
            m_FireRate.m_AmountGuns = 2;
        else if (m_CurrentFireRate > 2)
            m_FireRate.m_AmountGuns = 2;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomPower : MonoBehaviour
{
    [SerializeField] private GameObject m_HP;
    [SerializeField] private GameObject m_Guns;

    private Collider[] m_Colliders;

    private float m_Time;

    [SerializeField] private Bullet m_Bullet;

    private void Awake()
    {
        if (m_Bullet == null)
            Debug.LogError("No Bullet script found");                
    }

    private void Start()
    {
        m_HP.SetActive(false);
        m_Guns.SetActive(false);
    }

    void Update()
    {
        m_Time += Time.deltaTime;

        if (m_Time >= 8f && m_Time <= 8.1f)
        {
            StartCoroutine(HPBuff());
        }
        if (m_Time >= 16 && m_Time <= 16.1f)
        {
            StartCoroutine(GunBuff());
        }
    }
    private IEnumerator HPBuff()
    {
        m_HP.SetActive(true);
        m_HP.transform.position = new Vector2(Random.Range(-6.5f, 6.5f), Random.Range(-8, 6));
        yield return new WaitForSeconds(2f);
        m_HP.transform.position = transform.position;
        m_HP.SetActive(false);
    }
    private IEnumerator GunBuff()
    {
        m_Guns.SetActive(true);
        m_Guns.transform.position = new Vector2(Random.Range(-6.5f, 6.5f), Random.Range(-8, 6));
        yield return new WaitForSeconds(2f);
        m_Time = 0;
        m_Guns.transform.position = transform.position;
        m_Guns.SetActive(false);
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;
    private float m_MoveX, m_MoveY;

    private Rigidbody m_RB;

    public Boundary m_Bounds;

    void Start()
    {
        m_RB = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        
    }

    void FixedUpdate()
    {
        m_MoveX = Input.GetAxis("Horizontal");
        m_MoveY = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(m_MoveX, m_MoveY);

        transform.Translate(movement * Time.deltaTime * m_Speed);

        float zpos = transform.position.z;

        if(zpos != 0)
        {
            zpos = 0;
        }

        PositionCheck();
    }

    private void PositionCheck()
    {
        if (m_RB.position.x > m_Bounds.m_XMax)
        {
            transform.position = new Vector2(m_Bounds.m_XMin, m_RB.position.y);
        }
        if (m_RB.position.x < m_Bounds.m_XMin)
        {
            transform.position = new Vector2(m_Bounds.m_XMax, m_RB.position.y);
        }
        if (m_RB.position.y > m_Bounds.m_YMax)
        {
            transform.position = new Vector2(m_RB.position.x, m_Bounds.m_YMax);
        }
        if (m_RB.position.y < m_Bounds.m_YMin)
        {
            transform.position = new Vector2(m_RB.position.x, m_Bounds.m_YMin);
        }
    }

    public void AddMoveSpeed()
    {
        m_Speed = m_Speed + 5;
    }
}

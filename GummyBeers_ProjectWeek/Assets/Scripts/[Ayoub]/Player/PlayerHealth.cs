﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    private int m_MaxHealth;

    private float m_CurHealth;

    private Vector3 m_Deadpos;
    private float m_Counter = 0;

    [SerializeField] private Image m_HealthImage;

    private Boss m_Boss;

    private SaveScoreData m_ScoreDataToSave;

    void Start ()
    {
        m_Boss = GameObject.FindObjectOfType<Boss>();

        m_MaxHealth = UserData.m_Health;

        m_CurHealth = m_MaxHealth;

        if (m_HealthImage == null)
        {
            Debug.LogError("No Image found of health!");
        }

        m_ScoreDataToSave = GetComponent<SaveScoreData>();
	}
	
	void Update ()
    {
        m_HealthImage.fillAmount = m_CurHealth / 5;

        if (m_CurHealth <= 0)
        {
            // Player dies
            m_Boss.ResetDied();

            m_Deadpos = transform.position;

            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Fire>().enabled = false;
            GetComponent<Player>().enabled = false;

            m_ScoreDataToSave.SaveData();
        }
        
        if(m_CurHealth > m_MaxHealth)
        {
            m_CurHealth = m_MaxHealth;
        }

        Debug.Log(m_CurHealth);
	}

    public void TakingDamage(int damage)
    {
        m_CurHealth =  m_CurHealth - damage;
    }

    public void HealingHealth(int health)
    {
        m_CurHealth += health;
    }

    public float CheckHealth()
    {
        return m_CurHealth;
    }

    private void Respawn()
    {
        bool isalive = false;

        m_Counter += Time.deltaTime;

        if(m_Counter >= 3)
        {
            HealingHealth(5);

            isalive = true;

            transform.position = m_Deadpos;

            m_Counter = 0;
        }

        GetComponent<SpriteRenderer>().enabled = isalive;
        GetComponent<Fire>().enabled = isalive;
        GetComponent<Player>().enabled = isalive;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            m_CurHealth -= 1;

            EnemyPool enemy = other.GetComponent<EnemyPool>();

            enemy.TakingDamage(1);
        }

        if (other.tag == "Meteor")
        {
            m_CurHealth -= 1;

            Pool meteor = other.GetComponent<Pool>();

            meteor.ReturnToPool();
        }

        if (other.tag == "Lazer")
        {
            m_CurHealth -= 3;
        }

        if (other.tag == "HP")
        {
            m_CurHealth = m_MaxHealth;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyObjectPool : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_PoolObject = new List<GameObject>();

    [SerializeField]
    private int m_PoolSize;

    List<EnemyPoolItem> m_Pool;

    void Start()
    {
        m_Pool = new List<EnemyPoolItem>();

        GameObject m_Temp;
        for (int i = 0; i < m_PoolSize; i++)
        {
            int rand = Random.Range(0, m_PoolObject.Count);

            m_Temp = Instantiate(m_PoolObject[rand]);
            AddItem(m_Temp.GetComponent<EnemyPoolItem>());
            m_Pool[i].Pool = this;
        }
    }

    public void AddItem(EnemyPoolItem item)
    {
        m_Pool.Add(item);

        item.transform.parent = transform;
        item.gameObject.SetActive(false);
    }

    public GameObject InstantiateObject(Vector3 positiion, Quaternion rotation, Transform parent = null)
    {
        if (m_Pool.Count <= 0)
        {
            Debug.Log("tis leeg lol");
            return null;
        }

        m_Pool[0].Init(positiion, rotation, parent);
        GameObject item = m_Pool[0].gameObject;
        m_Pool.RemoveAt(0);
        return item;
    }
}

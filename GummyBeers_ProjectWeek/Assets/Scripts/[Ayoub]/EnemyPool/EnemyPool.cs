﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : EnemyPoolItem
{
    [SerializeField]
    private int m_Damage;

    [SerializeField]
    private int m_MaxHealthMultiplier;
    [SerializeField] private int m_MaxHealth;
    [SerializeField] private int m_CurHealth;

    [SerializeField]
    private GameObject m_DeathObject;

    private Boss m_Boss;

    private bool m_ReturnToPool = false;

    protected override void Restart()
    {
        m_ReturnToPool = false;
    }

    private void Start()
    {
        m_Boss = GameObject.FindObjectOfType<Boss>();

        if (m_Boss == null)
            Debug.LogError(gameObject.name + " Missing Object Score");

        m_ReturnToPool = false;
        if (m_MaxHealth < 1)
        {
            m_MaxHealth = 2;
            m_MaxHealth *= m_MaxHealthMultiplier;
        }

        m_CurHealth = m_MaxHealth;
    }

    private void Update()
    {
        if (m_CurHealth > m_MaxHealth)
        {
            m_CurHealth = m_MaxHealth;
        }

        if (m_ReturnToPool)
        {
            ReturnToPool();
        }

        if (transform.position.y < -9)
        {
            ReturnTrue();
        }

        if (m_CurHealth <= 0 && this.gameObject.name == "Boss")
        {
            UserData.m_Score += 50;

            m_Boss.Win();
        }
        else if (m_CurHealth <= 0)
        {
            UserData.m_Score += 10;

            m_Boss.AddAmount(0.1f);

            Instantiate(m_DeathObject);

            ReturnTrue();
        }
    }

    public bool ReturnTrue()
    {
        m_ReturnToPool = true;

        return m_ReturnToPool;
    }

    public void TakingDamage(int damage)
    {
        m_CurHealth -= damage;
    }

    public int EnemyHealth()
    {
        return m_CurHealth;
    }

    public void BossTakingDamage()
    {
        m_CurHealth -= 1;
        m_Boss.DamageTaken(0.01f);
    }
}
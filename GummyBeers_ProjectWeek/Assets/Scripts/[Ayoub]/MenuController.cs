﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour 
{
    [SerializeField]
    private GameObject m_MainActive, m_CreateActive, m_LoginActive;

	// Use this for initialization
	void Start () 
	{
        m_MainActive.SetActive(true);

        m_CreateActive.SetActive(false);

        m_LoginActive.SetActive(false);
	}

    public void MainUI()
    {
        m_MainActive.SetActive(true);

        m_CreateActive.SetActive(false);

        m_LoginActive.SetActive(false);
    }
	
	public void CreateUI()
    {
        m_MainActive.SetActive(false);

        m_CreateActive.SetActive(true);

        m_LoginActive.SetActive(false);
    }

    public void LoginUI()
    {
        m_MainActive.SetActive(false);

        m_CreateActive.SetActive(false);

        m_LoginActive.SetActive(true);
    }
}

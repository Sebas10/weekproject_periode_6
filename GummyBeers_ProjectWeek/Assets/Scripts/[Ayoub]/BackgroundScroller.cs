﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    [SerializeField]
    private float m_ScrollingSpeed;

    private Vector2 m_StartPOS;

    // Start is called before the first frame update
    void Start()
    {
        m_StartPOS = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0, -1, 0) * m_ScrollingSpeed * Time.deltaTime);

        if(transform.position.y < -27.2f)
        {
            transform.position = m_StartPOS;
        }
    }
}

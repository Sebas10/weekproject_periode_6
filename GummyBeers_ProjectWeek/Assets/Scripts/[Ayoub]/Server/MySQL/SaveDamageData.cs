﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDamageData : MonoBehaviour
{
    public void SaveData()
    {
        StartCoroutine(SaveUserData());
    }

    private IEnumerator SaveUserData()
    {
        WWWForm form = new WWWForm();

        form.AddField("name", UserData.m_Username);
        form.AddField("damage", UserData.m_Damage);

        WWW www = new WWW("http://127.0.0.1/edsa-Example/UpdateDamage.php", form);

        yield return www;

        if (www.error != null)
        {

        }
        else
        {
            DamageDataToHold data = JsonUtility.FromJson<DamageDataToHold>(www.text);

            Debug.Log("Data successfully saved");
        }
    }

    public void AddDamage()
    {
        UserData.m_Damage += 1;

        SaveData();
    }
}

[System.Serializable]
public class DamageDataToHold
{
    public int m_Damage;
}

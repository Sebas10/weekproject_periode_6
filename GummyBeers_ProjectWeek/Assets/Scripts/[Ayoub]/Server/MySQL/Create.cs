﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Create : MonoBehaviour 
{
    private TextMeshProUGUI m_Text;

    [SerializeField]
    private InputField m_UsernameInput, m_EmailInput, m_PasswordInput;

    public void CheckAccount()
    {
        StartCoroutine(create());
    }

    private IEnumerator create()
    {
        WWWForm form = new WWWForm();

        form.AddField("name", m_UsernameInput.text);
        form.AddField("email", m_EmailInput.text);
        form.AddField("password", m_PasswordInput.text);

        WWW www = new WWW("http://127.0.0.1/edsa-Example/createunity.php", form);

        yield return www;

        if (www.error != null)
        {
            m_Text.text = www.error;
        }
        else
        {
            m_Text.text = "Account has been created";
        }
    }
}

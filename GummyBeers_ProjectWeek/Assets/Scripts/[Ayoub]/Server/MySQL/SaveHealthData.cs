﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveHealthData : MonoBehaviour
{
    public void SaveData()
    {
        StartCoroutine(SaveUserData());
    }

    private IEnumerator SaveUserData()
    {
        WWWForm form = new WWWForm();

        form.AddField("name", UserData.m_Username);
        form.AddField("health", UserData.m_Health);

        WWW www = new WWW("http://127.0.0.1/edsa-Example/UpdateHealth.php", form);

        yield return www;

        if (www.error != null)
        {

        }
        else
        {
            HealthDataToHold data = JsonUtility.FromJson<HealthDataToHold>(www.text);

            Debug.Log("Data successfully saved");
        }
    }

    public void AddHealth()
    {
        UserData.m_Health += 5;

        SaveData();
    }
}

[System.Serializable]
public class HealthDataToHold
{
    public int m_Health;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UserData
{
    public static string m_Username;

    public static int m_Score;
    public static int m_Health;
    public static int m_Damage;

    public static float m_Speed;
    public static float m_FireRate;

    public static bool LoggedIn { get { return m_Username != null; } }
}

[System.Serializable]
public class BaseResponse
{
    public int score;
    public int health;
    public int damage;
}
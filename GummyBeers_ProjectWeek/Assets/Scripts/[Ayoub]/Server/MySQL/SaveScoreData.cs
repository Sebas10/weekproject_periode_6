﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveScoreData : MonoBehaviour
{
    public void SaveData()
    {
        StartCoroutine(SaveUserData());
    }

    private IEnumerator SaveUserData()
    {
        WWWForm form = new WWWForm();

        form.AddField("name", UserData.m_Username);
        form.AddField("score", UserData.m_Score);

        WWW www = new WWW("http://127.0.0.1/edsa-Example/UpdateScore.php", form);

        yield return www;

        if (www.error != null)
        {

        }
        else
        {
            ScoreDataToHold data = JsonUtility.FromJson<ScoreDataToHold>(www.text);

            Debug.Log("Data successfully saved");

            SceneManager.LoadScene("Upgrade");
        }
    }
}

[System.Serializable]
public class ScoreDataToHold
{
    public int m_Score;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    private TMPro.TextMeshProUGUI m_Text;

    [SerializeField]
    private InputField m_UsernameInput, m_PasswordInput;

    public void CheckAccount()
    {
        StartCoroutine(login());
    }

    private IEnumerator login()
    {
        WWWForm form = new WWWForm();

        form.AddField("name", m_UsernameInput.text);
        form.AddField("password", m_PasswordInput.text);

        WWW www = new WWW("http://127.0.0.1/edsa-Example/login.php", form);

        yield return www;

        if (www.error != null)
        {
            m_Text.text = www.error;

            Debug.Log("Can't login");
        }
        else
        {
            LoginResponse data = JsonUtility.FromJson<LoginResponse>(www.text);

            UserData.m_Username = m_UsernameInput.text;

            BaseResponse basedata = JsonUtility.FromJson<BaseResponse>(www.text);

            UserData.m_Score = basedata.score;
            UserData.m_Health = basedata.health;
            UserData.m_Damage = basedata.damage;

            SceneManager.LoadScene("Upgrade");
        }
    }
}

[System.Serializable]
public class LoginResponse
{
    public string token;
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateEnemyShip : MonoBehaviour
{
    [SerializeField]
    private ObjectPool m_EnemyToSpawn;

    [SerializeField]
    private float m_TimerToSpawn;

    private float m_SpawnX;

    private float m_SpawnCounter;

    // Use this for initialization
    void Update()
    {
        m_SpawnCounter += Time.deltaTime;

        if (m_SpawnCounter > m_TimerToSpawn)
        {
            m_SpawnX = Random.Range(-6, 6);

            transform.position = new Vector3(m_SpawnX, 15, 0f);

            m_EnemyToSpawn.InstantiateObject(transform.position, Quaternion.identity);

            m_SpawnCounter = 0;
        }
    }
}

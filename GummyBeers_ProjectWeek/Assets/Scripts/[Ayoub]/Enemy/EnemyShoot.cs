﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour {
    
    [SerializeField]
    private ObjectPool m_BulletPool;

    [SerializeField]
    private float m_MaxFireRate;

    private float m_CurFireRate;

    private void Start()
    {
        m_CurFireRate = 0f;
    }

    // Update is called once per frame
    void Update ()
    {
        m_CurFireRate += Time.deltaTime;

        if(m_CurFireRate >= m_MaxFireRate)
        {
            m_BulletPool.InstantiateObject(transform.position, Quaternion.identity);

            m_CurFireRate = 0f;
        }
	}
}

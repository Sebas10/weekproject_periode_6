﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : PoolItem
{
    [SerializeField]
    private int m_Damage;

    private bool m_ReturnToPool = false;

    protected override void Restart()
    {
        m_ReturnToPool = false;
    }

    private void Start()
    {
        m_ReturnToPool = false;
    }

    private void Update()
    {
        if (m_ReturnToPool)
        {
            ReturnToPool();
        }

        if (transform.position.y < -9)
        {
            m_ReturnToPool = true;
        }
    }

    public bool ReturnTrue()
    {
        m_ReturnToPool = true;

        return m_ReturnToPool;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary : MonoBehaviour
{
	public float m_XMin, m_XMax, m_YMin, m_YMax;
}

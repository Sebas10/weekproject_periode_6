﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameHolder : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI m_PlayerNameHolder, m_ScoreData, m_HealthData, m_DamageData;

    // Start is called before the first frame update
    void Start()
    {
        if (UserData.LoggedIn)
        {
            m_PlayerNameHolder.text = UserData.m_Username;

            //m_ScoreData.text = "" + UserData.m_Score;

            //m_HealthData.text = "" + UserData.m_Health;

            //m_DamageData.text = "" + UserData.m_Damage;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (UserData.LoggedIn)
        {
            m_ScoreData.text = "" + UserData.m_Score;

            m_HealthData.text = "" + UserData.m_Health;

            m_DamageData.text = "" + UserData.m_Damage;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField]
    private float m_Speed;

    private void Update()
    {
        transform.Translate(Vector3.up * (m_Speed * Time.deltaTime));
    }
}
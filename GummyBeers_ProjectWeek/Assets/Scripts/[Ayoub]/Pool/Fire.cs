﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShootingState
    {
        OnePoint = 0,
        TwoPoints,
        ThreePoints
    }

public class Fire : MonoBehaviour {
    
    public ObjectPool m_BulletPool;

    [SerializeField]
    private float m_MaxFireRate;

    private float m_CurFireRate;

    public int m_AmountGuns = 0;

    private ShootingState m_ShootingState;

    [SerializeField]
    private Transform m_ShootingPoint1;

    [SerializeField]
    private List<Transform> m_Shootingpoint2 = new List<Transform>();

    [SerializeField]
    private List<Transform> m_Shootingpoint3 = new List<Transform>();

    [SerializeField]
    private AudioClip m_ShootClip;
    
    private AudioSource m_ShootSource;

    private void Start()
    {
        m_CurFireRate = m_MaxFireRate;

        m_ShootSource = GetComponent<AudioSource>();

        m_ShootSource.clip = m_ShootClip;
    }

    void Update ()
    {
        m_CurFireRate += Time.deltaTime;

        switch (m_AmountGuns)
        {
            case 0:
                m_ShootingState = ShootingState.OnePoint;
                break;

            case 1:
                m_ShootingState = ShootingState.TwoPoints;
                break;

            case 2:
                m_ShootingState = ShootingState.ThreePoints;
                break;

            default:
                m_ShootingState = ShootingState.OnePoint;
                break;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (m_CurFireRate >= m_MaxFireRate)
            {
                switch (m_ShootingState)
                {
                    case (ShootingState.OnePoint):

                        ShootOnePoint();

                        break;

                    case (ShootingState.TwoPoints):

                        ShootTwoPoints();

                        break;

                    case (ShootingState.ThreePoints):

                        ShootThreePoints();

                        break;
                }

                m_CurFireRate = 0;

                m_ShootSource.Play();
            }
        }
	}

    private void ShootOnePoint()
    {
        m_BulletPool.InstantiateObject(m_ShootingPoint1.position, Quaternion.identity);
    }

    private void ShootTwoPoints()
    {
        for(int i = 0; i < m_Shootingpoint2.Count; i++)
        {
            m_BulletPool.InstantiateObject(m_Shootingpoint2[i].position, Quaternion.identity);
        }
    }

    private void ShootThreePoints()
    {
        for (int i = 0; i < m_Shootingpoint3.Count; i++)
        {
            m_BulletPool.InstantiateObject(m_Shootingpoint3[i].position, m_Shootingpoint3[i].rotation);
        }
    }
}
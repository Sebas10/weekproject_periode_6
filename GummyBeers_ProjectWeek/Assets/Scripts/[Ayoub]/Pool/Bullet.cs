﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : PoolItem
{
    [SerializeField]
    private Player m_Player;

    private int m_Damage;

    public float m_BulletSpeed;

    private float m_TimerBuff;

    private float m_DeathTimer;

    private Boundary m_Bounds;    

    protected override void Restart()
    {
        m_DeathTimer = 0.0f;

        m_Bounds = FindObjectOfType<Boundary>();

        m_Damage = UserData.m_Damage;
    }

    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * m_BulletSpeed);

        m_DeathTimer += Time.deltaTime;

        PositionCheck();

        if (m_DeathTimer >= 3.0f)
        {
            ReturnToPool();
        }

        Debug.Log(m_Damage);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Meteor")
        {
            UserData.m_Score += 5;

            Pool meteor = other.GetComponent<Pool>();

            meteor.ReturnToPool();

            ReturnToPool();
        }

        if (other.tag == "Enemy")
        {
            UserData.m_Score += 10;

            EnemyPool enemy = other.GetComponent<EnemyPool>();

            enemy.TakingDamage(m_Damage);

            ReturnToPool();
        }

        if (other.tag == "Boss")
        {
            UserData.m_Score += 2;

            EnemyPool enemy = other.GetComponent<EnemyPool>();

            enemy.BossTakingDamage();

            ReturnToPool();
        }
    }

    private void PositionCheck()
    {
        if (transform.position.y > m_Bounds.m_YMax + 0.5f)
        {
            ReturnToPool();
        }
    }
}
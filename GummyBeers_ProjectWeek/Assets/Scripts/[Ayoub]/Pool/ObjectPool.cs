﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    [SerializeField]
    private GameObject m_PoolObject;

    [SerializeField]
    private int m_PoolSize;

    //[SerializeField]
    //private AudioClip m_ShootClip;

    //[SerializeField]
    //private AudioSource m_ShootSource;

    List<PoolItem> m_Pool;
    
	void Start ()
    {
        m_Pool = new List<PoolItem>();

        GameObject m_Temp;
        for (int i = 0; i < m_PoolSize; i++)
        {
            m_Temp = Instantiate(m_PoolObject);
            AddItem(m_Temp.GetComponent<PoolItem>());
            m_Pool[i].Pool = this;
        }
	}

    public void AddItem(PoolItem item)
    {
        m_Pool.Add(item);

        item.transform.parent = transform;
        item.gameObject.SetActive(false);
    }

    public GameObject InstantiateObject(Vector3 positiion, Quaternion rotation, Transform parent = null)
    {
        if(m_Pool.Count <= 0)
        {
            Debug.Log("tis leeg lol");
            return null;
        }

        m_Pool[0].Init(positiion, rotation, parent);
        GameObject item = m_Pool[0].gameObject;
        m_Pool.RemoveAt(0);
        return item;
    }
}

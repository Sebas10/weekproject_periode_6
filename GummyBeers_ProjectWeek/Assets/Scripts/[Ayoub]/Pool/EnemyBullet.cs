﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : PoolItem
{
    private float m_DeathTimer;

    [SerializeField]
    private int m_Damage;

    protected override void Restart()
    {
        m_DeathTimer = 0.0f;
    }

    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * 7.0f);

        m_DeathTimer += Time.deltaTime;

        if (m_DeathTimer >= 3.0f)
        {
            ReturnToPool();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerHealth playerhealth = other.GetComponent<PlayerHealth>();

            playerhealth.TakingDamage(m_Damage);

            m_DeathTimer = 3.0f;
        }
    }
}
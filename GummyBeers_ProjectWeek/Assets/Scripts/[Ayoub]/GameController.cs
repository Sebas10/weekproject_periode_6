﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_Objects = new List<GameObject>();

    [SerializeField]
    private PlayableDirector m_PlayableDirector;

    private bool m_GameReady = false;

    private Fire m_Fire;

    // Start is called before the first frame update
    void Start()
    {
        m_Fire = FindObjectOfType<Fire>();

        m_Fire.GetComponent<Fire>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < m_Objects.Count; i++)
        {
            m_Objects[i].SetActive(m_GameReady);
        }

        if (m_PlayableDirector.state != PlayState.Playing)
        {
            m_GameReady = true;

            m_Fire.GetComponent<Fire>().enabled = true;
        }
    }
}
